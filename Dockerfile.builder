FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install -y axel git mercurial curl wget axel
RUN axel -n 10 'https://storage.googleapis.com/golang/go1.6.2.linux-amd64.tar.gz'
RUN tar xzvf go1.6.2.linux-amd64.tar.gz && mv go /usr/local
RUN echo 'export GOPATH=/app' >> /root/.bashrc
RUN echo 'export PATH=/usr/local/go/bin:$PATH' >> /root/.profle
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g ionic@beta
RUN npm install -g cordova
VOLUME ["/app"]