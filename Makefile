docker_builder_build:
	docker build -t dotpulsard_builder:0.1 -f Dockerfile.builder .

docker_builder_run: docker_builder_build
	docker run --rm -v $(shell pwd):/app -it dotpulsard_builder:0.1 bash /app/scripts/build.sh

docker_builder_shell: docker_builder_build
	docker run --rm -v $(shell pwd):/app -it dotpulsard_builder:0.1 bash

docker_builder_mobile: docker_builder_build
	docker run --rm -v $(shell pwd):/app -p 8100:8100 -p 35729:35729 -it dotpulsard_builder:0.1 bash /app/scripts/mobile_serve.sh

docker_build:
	docker build -t dotpulsard:0.1 -f Dockerfile .

docker_run: docker_builder_run
	docker run --rm -v $(shell pwd)/data:/app/data -v $(shell pwd)/data_db:/var/lib/postgresql -p 33022:22 -p 33080:8080 -it dotpulsard:0.1 bash /app/scripts/run.sh

docker_rundev: docker_builder_run
	docker run --rm -v $(shell pwd):/app -v $(shell pwd)/data_db:/var/lib/postgresql -p 33022:22 -p 33080:8080 -it dotpulsard:0.1 bash /app/scripts/rundev.sh

devrun: docker_rundev