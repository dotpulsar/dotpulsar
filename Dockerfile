FROM ubuntu:16.04

RUN apt-get update -y && \
	apt-get install -y apt-transport-https curl wget axel

RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' > /etc/apt/sources.list.d/pgdg.list && \
	wget -qO- https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

#RUN wget https://dl.influxdata.com/influxdb/releases/influxdb_0.13.0_amd64.deb && \
#	dpkg -i influxdb_0.13.0_amd64.deb

RUN echo "deb https://repos.influxdata.com/ubuntu xenial stable" > /etc/apt/sources.list.d/influxdb.list && \
	wget -qO- https://repos.influxdata.com/influxdb.key | apt-key add -

RUN apt-get update -y && \
	apt-get install -y influxdb openssh-server

RUN ln -s /app/data/db /var/lib/postgresql

RUN mkdir -p /app/scripts
COPY bin/dotpulsard /app/dotpulsard
COPY scripts/run.sh /app/scripts/run.sh
