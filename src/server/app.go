package server

import (
	"log"
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
)

type App struct {
	Config Config
	Router *echo.Echo
}

func NewApp(config Config) *App {
	app := &App{
		Config: config,
		Router: echo.New(),
	}
	app.Router.Use(middleware.Logger())
	app.Router.Use(middleware.Recover())
	//app.Router.Use(app.needAuthChecker)
	app.Router.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte("secret"),
		TokenLookup: "query:token",
	}))
	app.Router.Get("/", app.handlerIndex)
	app.Router.Static("/", app.Config.StaticPath)
	return app
}

func (app *App) Run() error {
	log.Printf("listen on %s", app.Config.ListenAddr)
	app.Router.Run(standard.New(app.Config.ListenAddr))
	return nil
}

func (app *App) needAuthChecker(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		//if ctx.Cookie(ctx.Path()
		return nil
	}
}