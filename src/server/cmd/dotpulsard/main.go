package main

import (
	"flag"
	"log"

	"server"
)

func main() {
	flListenAddr := flag.String("listen-addr", "0.0.0.0:8080", "listen addr:port")
	flStaticPath := flag.String("static-path", "./static", "path to the web static files dir")
	flViewPath := flag.String("view-path", "./view", "path to the view template dir")
	flag.Parse()
	app := server.NewApp(server.Config{
		ListenAddr: *flListenAddr,
		StaticPath: *flStaticPath,
		ViewPath:   *flViewPath,
	})
	log.Printf("starting...")
	err := app.Run()
	if err != nil {
		log.Fatalf("app run err: %s", err)
	}
}