package server

import (
	"net/http"
	"github.com/labstack/echo"
)

func (app *App) handlerIndex(ctx echo.Context) error {
	return ctx.String(http.StatusOK, "DotPulsard API")
}