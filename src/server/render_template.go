package server

import (
	"fmt"
	"github.com/labstack/echo"
	"gopkg.in/flosch/pongo2.v3"
)

func (app *App) renderTemplate(ctx echo.Context, name string, args...interface{}) error {
	path := fmt.Sprintf("%s/%s.html", app.Config.ViewPath, name)
	tpl, err := pongo2.FromFile(path)
	if err != nil {
		return err
	}
	var tplContext pongo2.Context
	for i:=0; i<len(args); i+=2 {
		tplContext[args[i].(string)] = args[i+1]
	}
	return tpl.ExecuteWriter(tplContext, ctx.Response())
}