package server

type Config struct {
	ListenAddr string
	ViewPath   string
	StaticPath string
}