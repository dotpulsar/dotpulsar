package server

import (
	"github.com/labstack/echo"
)

func (app *App) handlerLogin(ctx echo.Context) error {
	return app.renderTemplate(ctx, "login")
}