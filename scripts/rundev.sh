#!/bin/bash

apt-get install -y postgresql-9.5

service influxdb start
service ssh start
service postgresql start

/app/bin/dotpulsard