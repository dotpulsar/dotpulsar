#!/bin/bash

export GOPATH=/app
GO=/usr/local/go/bin/go
export GOPATH=/app

cd /app

${GO} get -v server/cmd/dotpulsard && \
	${GO} build -v -o bin/serverd server/cmd/dotpulsard